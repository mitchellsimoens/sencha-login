module.exports = {
    get Adapter () {
        return require('./Adapter');
    },

    get combiner () {
        return require('./combiner');
    },

    get Forum () {
        return require('./Forum');
    },

    get Login () {
        return require('./Login');
    },

    get Manager () {
        return require('./Manager');
    },

    get Sencha () {
        return require('./Sencha');
    }
};
